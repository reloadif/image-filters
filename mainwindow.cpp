#include <QImage>
#include <QMessageBox>
#include <QDir>

#include "mainwindow.h"
#include "ui_mainwindow.h"

// Base filters
#include "filter/base/base.hpp"
#include "filter/base/matrix_base.hpp"

// Linear filters
#include "filter/linear/brightness.hpp"
#include "filter/linear/glass.hpp"
#include "filter/linear/gray-world.hpp"
#include "filter/linear/grayscale.hpp"
#include "filter/linear/invert.hpp"
#include "filter/linear/median.hpp"
#include "filter/linear/perfect-reflector.hpp"
#include "filter/linear/rotate.hpp"
#include "filter/linear/sepia.hpp"
#include "filter/linear/transfer.hpp"
#include "filter/linear/waves_x.hpp"
#include "filter/linear/waves_y.hpp"

// Matrix filters
#include "filter/matrix/blur.hpp"
#include "filter/matrix/embossing.hpp"
#include "filter/matrix/gaussian_blur.hpp"
#include "filter/matrix/harshness.hpp"
#include "filter/matrix/motion-blur.hpp"
#include "filter/matrix/pruitt_x.hpp"
#include "filter/matrix/pruitt_y.hpp"
#include "filter/matrix/scharr_x.hpp"
#include "filter/matrix/scharr_y.hpp"
#include "filter/matrix/sharpness.hpp"
#include "filter/matrix/sobel_x.hpp"
#include "filter/matrix/sobel_y.hpp"

QImage currentImage;
QList<BaseFilter*> filters = QList<BaseFilter*>();

void InitializationFilters() {
    // Linear filters
    filters.append(new BrightnessFilter());
    filters.append(new GlassFilter());
    filters.append(new GrayWorldFilter());
    filters.append(new GrayScaleFilter());
    filters.append(new InvertFilter());
    filters.append(new MedianFilter());
    filters.append(new PerfectReflectorFilter());
    filters.append(new RotateFilter(60));
    filters.append(new SepiaFilter());
    filters.append(new TransferFilter(300));
    filters.append(new Waves_X());
    filters.append(new Waves_Y());

    // Matrix filters
    filters.append(new BlurFilter());
    filters.append(new EmbossingFilter());
    filters.append(new GaussianBlurFilter());
    filters.append(new HarshnessFilter());
    filters.append(new MotionBlurFilter());
    filters.append(new Pruitt_X());
    filters.append(new Pruitt_Y());
    filters.append(new Scharr_X());
    filters.append(new Scharr_Y());
    filters.append(new SharpnessFilter());
    filters.append(new Sobel_X());
    filters.append(new Sobel_Y());
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QIcon icon("Logo.png");
    setWindowIcon(icon);

    QDir::current().mkdir("image");

    statusBar()->setSizeGripEnabled(false);

    ui->applyFilter->setEnabled(false);
    ui->applyAllFilters->setEnabled(false);

    InitializationFilters();

    for (auto filter : filters) {
        ui->listOfFilters->addItem(filter->getName());
    }

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_addImage_clicked()
{
    QString URL = ui->URL_Input->text();
    QImage image;

    if ( image.load(URL) ) {
        ui->addImage->setEnabled(false);
        ui->applyFilter->setEnabled(true);
        ui->applyAllFilters->setEnabled(true);

        currentImage = image;
        image.save("images/start.png");
        ui->imageOut->setPixmap(QPixmap::fromImage(image.scaled(750,550)));

        ui->addImage->setEnabled(true);
    }
    else {
        QMessageBox::warning(this, "Ошибка", "Путь указан не верно!");
    }

}

void MainWindow::on_applyFilter_clicked()
{
    ui->addImage->setEnabled(false);
    ui->applyFilter->setEnabled(false);
    ui->applyAllFilters->setEnabled(false);

    int currentItem = ui->listOfFilters->currentRow();
    if (currentItem == 9 || currentItem == 10) {
        if(currentItem == 9) {
            PerfectReflectorFilter* filter = dynamic_cast<PerfectReflectorFilter*>(filters[currentItem]);
            filter->calculateFields(currentImage);
        }
        else {
            GrayWorldFilter* filter = dynamic_cast<GrayWorldFilter*>(filters[currentItem]);
            filter->calculateFields(currentImage);
        }
    }
    QImage image = filters[currentItem]->processImage(currentImage);
    image.save("images/"+filters[currentItem]->getName()+".png");
    ui->imageOut->setPixmap(QPixmap::fromImage(image.scaled(750,550)));

    ui->addImage->setEnabled(true);
    ui->applyFilter->setEnabled(true);
    ui->applyAllFilters->setEnabled(true);
}


void MainWindow::on_applyAllFilters_triggered()
{
    ui->addImage->setEnabled(false);
    ui->applyFilter->setEnabled(false);
    ui->applyAllFilters->setEnabled(false);

    for (int i = 0; i < filters.length(); i++) {
        if (i == 2 || i == 6) {
            if (i == 2) {
                GrayWorldFilter* filter = dynamic_cast<GrayWorldFilter*>(filters[i]);
                filter->calculateFields(currentImage);
            }
            else {
                PerfectReflectorFilter* filter = dynamic_cast<PerfectReflectorFilter*>(filters[i]);
                filter->calculateFields(currentImage);
            }
        }

        filters[i]->processImage(currentImage).save("./images/"+filters[i]->getName()+".png");;
    }

    ui->addImage->setEnabled(true);
    ui->applyFilter->setEnabled(true);
    ui->applyAllFilters->setEnabled(true);
}
