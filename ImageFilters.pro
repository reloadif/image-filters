QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    filter/base/base.hpp \
    filter/base/matrix_base.hpp \
    filter/linear/brightness.hpp \
    filter/linear/glass.hpp \
    filter/linear/gray-world.hpp \
    filter/linear/grayscale.hpp \
    filter/linear/invert.hpp \
    filter/linear/median.hpp \
    filter/linear/perfect-reflector.hpp \
    filter/linear/rotate.hpp \
    filter/linear/sepia.hpp \
    filter/linear/transfer.hpp \
    filter/linear/waves_x.hpp \
    filter/linear/waves_y.hpp \
    filter/matrix/blur.hpp \
    filter/matrix/embossing.hpp \
    filter/matrix/gaussian_blur.hpp \
    filter/matrix/harshness.hpp \
    filter/matrix/motion-blur.hpp \
    filter/matrix/pruitt_x.hpp \
    filter/matrix/pruitt_y.hpp \
    filter/matrix/scharr_x.hpp \
    filter/matrix/scharr_y.hpp \
    filter/matrix/sharpness.hpp \
    filter/matrix/sobel_x.hpp \
    filter/matrix/sobel_y.hpp \
    mainwindow.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
