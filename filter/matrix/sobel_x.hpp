#ifndef SOBEL_X_HPP
#define SOBEL_X_HPP

#include "../base/matrix_base.hpp"

class Sobel_X : public MatrixBaseFilter {
public:
    Sobel_X();

    QString getName() override;
};

Sobel_X::Sobel_X() : MatrixBaseFilter(1, new float[9]{-1,0,1,-2,0,2,-1,0,1} ) {

}

QString Sobel_X::getName() {
    return QString("Sobel X");
}

#endif // SOBEL_X_HPP
