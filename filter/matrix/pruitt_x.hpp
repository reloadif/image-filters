#ifndef PRUITT_X_HPP
#define PRUITT_X_HPP

#include "../base/matrix_base.hpp"

class Pruitt_X : public MatrixBaseFilter {
public:
    Pruitt_X();

    QString getName() override;
};

Pruitt_X::Pruitt_X() : MatrixBaseFilter(1, new float[9]{-1,0,1,-1,0,1,-1,0,1} ) {

}

QString Pruitt_X::getName() {
    return QString("Pruitt X");
}

#endif // PRUITT_X_HPP
