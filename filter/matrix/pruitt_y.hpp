#ifndef PRUITT_Y_HPP
#define PRUITT_Y_HPP

#include "../base/matrix_base.hpp"

class Pruitt_Y : public MatrixBaseFilter {
public:
    Pruitt_Y();

    QString getName() override;
};

Pruitt_Y::Pruitt_Y() : MatrixBaseFilter(1, new float[9]{-1,-1,-1,0,0,0,1,1,1} ) {

}

QString Pruitt_Y::getName() {
    return QString("Pruitt Y");
}

#endif // PRUITT_Y_HPP
