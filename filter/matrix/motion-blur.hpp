#ifndef MOTIONBLURFILTER_HPP
#define MOTIONBLURFILTER_HPP

#include "../base/matrix_base.hpp"

class MotionBlurFilter : public MatrixBaseFilter {
    float* createArray();
public:
    MotionBlurFilter(const int radius = 3);

    QString getName() override;
};

MotionBlurFilter::MotionBlurFilter(const int radius) : MatrixBaseFilter(radius) {
    int size = 2 * radiusCore + 1;

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++){
            if(i == j) arrayOfData[size*i + j] = 1.f / size;
            else arrayOfData[size*i + j] = 0;
        }
    }

}

QString MotionBlurFilter::getName() {
    return QString("Motion blur");
}

#endif // MOTIONBLURFILTER_HPP
