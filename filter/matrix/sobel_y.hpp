#ifndef SOBEL_Y_HPP
#define SOBEL_Y_HPP

#include "../base/matrix_base.hpp"

class Sobel_Y : public MatrixBaseFilter {
public:
    Sobel_Y();

    QString getName() override;
};

Sobel_Y::Sobel_Y() : MatrixBaseFilter(1, new float[9]{-1,-2,-1,0,0,0,1,2,1} ) {

}

QString Sobel_Y::getName() {
    return QString("Sobel Y");
}

#endif // SOBEL_Y_HPP
