#ifndef BLURFILTER_HPP
#define BLURFILTER_HPP

#include "../base/matrix_base.hpp"

class BlurFilter : public MatrixBaseFilter {
public :
    BlurFilter(const int radius = 1);

    QString getName() override;
};


BlurFilter::BlurFilter(const int radius) : MatrixBaseFilter(radius) {
    int size = 2 * radiusCore + 1;

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            arrayOfData[i*size + j] = (float) 1 / (size * size);
        }
    }
}

QString BlurFilter::getName() {
    return QString("Blur");
}

#endif // BLURFILTER_HPP
