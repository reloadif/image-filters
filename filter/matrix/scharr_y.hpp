#ifndef SCHARR_Y_HPP
#define SCHARR_Y_HPP

#include "../base/matrix_base.hpp"

class Scharr_Y : public MatrixBaseFilter {
public:
    Scharr_Y();

    QString getName() override;
};

Scharr_Y::Scharr_Y() : MatrixBaseFilter(1, new float[9]{3,10,3,0,0,0,-3,-10,-3} ) {

}

QString Scharr_Y::getName() {
    return QString("Scharr Y");
}

#endif // SCHARR_Y_HPP
