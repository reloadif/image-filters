#ifndef EMBOSSINGFILTER_HPP
#define EMBOSSINGFILTER_HPP

#include "../base/matrix_base.hpp"

class EmbossingFilter : public MatrixBaseFilter {
public:
    EmbossingFilter();

    QString getName() override;
};

EmbossingFilter::EmbossingFilter() : MatrixBaseFilter(1, new float[9]{0,1,0,1,0,-1,0,-1,0}) {

}

QString EmbossingFilter::getName() {
    return QString("Embossing");
}

#endif // EMBOSSINGFILTER_H
