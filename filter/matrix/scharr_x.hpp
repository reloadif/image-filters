#ifndef SCHARR_X_HPP
#define SCHARR_X_HPP

#include "../base/matrix_base.hpp"

class Scharr_X : public MatrixBaseFilter {
public:
    Scharr_X();

    QString getName() override;
};

Scharr_X::Scharr_X() : MatrixBaseFilter(1, new float[9]{3,0,-3,10,0,-10,3,0,-3} ) {

}

QString Scharr_X::getName() {
    return QString("Scharr X");
}

#endif // SCHARR_X_HPP
