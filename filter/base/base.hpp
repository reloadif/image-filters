#ifndef BASEFILTER_HPP
#define BASEFILTER_HPP

#include <QImage>

class BaseFilter {
protected:
    virtual QColor calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const = 0;
public:
    virtual ~BaseFilter() = default;
    virtual QImage processImage(const QImage& image) const;

    virtual QString getName() = 0;
};

QImage BaseFilter::processImage(const QImage &image) const {
    QImage resultImage(image);

    for (int x = 0; x < image.width(); x++) {
        for (int y = 0; y < image.height(); y++) {
            QColor calculatedColor = calculateNewPixelColor(image, x, y);
            resultImage.setPixelColor(x, y, calculatedColor);
        }
    }

    return resultImage;
}


// Шаблонная функция для ограничения значений
template <typename T>
T clamp(T value, T min,T max) {
    if (value > max) return max;
    else if (value < min) return min;
    return value;
}

#endif // BASEFILTER_HPP
