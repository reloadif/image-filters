#ifndef SEPIAFILTER_HPP
#define SEPIAFILTER_HPP

#include "../base/base.hpp"

class SepiaFilter : public BaseFilter {
public :
    QColor calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const override;

    QString getName() override;
};

QColor SepiaFilter::calculateNewPixelColor (const QImage& image, int coordinateX, int coordinateY) const {
    QColor resultColor = image.pixelColor( coordinateX, coordinateY );
    double intensity =  0.36 * resultColor.red() + 0.53 * resultColor.green() + 0.11 * resultColor.blue();

    resultColor.setRgb( clamp<int>(intensity + 2 * 30, 0, 255), clamp<int>(intensity + 0.5 * 30, 0, 255), clamp<int>(intensity - 1 * 30, 0, 255) );
    return resultColor;
}

QString SepiaFilter::getName() {
    return QString("Sepia");
}

#endif // SEPIAFILTER_HPP
