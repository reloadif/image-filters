#ifndef GRAYSCALEFILTER_HPP
#define GRAYSCALEFILTER_HPP

#include "../base/base.hpp"

class GrayScaleFilter : public BaseFilter {
public :
    QColor calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const override;

    QString getName() override;
};

QColor GrayScaleFilter::calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const {
    QColor resultColor = image.pixelColor( coordinateX, coordinateY );
    resultColor.setRgb( 0.36 * resultColor.red() + 0.53 * resultColor.green() + 0.11 * resultColor.blue(),
                        0.36 * resultColor.red() + 0.53 * resultColor.green() + 0.11 * resultColor.blue(),
                        0.36 * resultColor.red() + 0.53 * resultColor.green() + 0.11 * resultColor.blue() );

    return resultColor;
}

QString GrayScaleFilter::getName() {
    return QString("Grayscale");
}

#endif // GRAYSCALEFILTER_HPP
