#ifndef WAVES_Y_HPP
#define WAVES_Y_HPP

#include "../base/base.hpp"

class Waves_Y : public BaseFilter {
public:
    QColor calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const override;

    QString getName() override;
};

QColor Waves_Y::calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const {
    const double PI = 3.141592653589793;

    int coordinateNewX = coordinateX + 20 * sin((2 * PI * coordinateX) / 30);

    return image.pixelColor( clamp<int>(coordinateNewX, 0, image.width()-1 ), coordinateY);
}

QString Waves_Y::getName() {
    return QString("Waves Y");
}

#endif // WAVES_Y_HPP
