#ifndef ROTATEFILTER_HPP
#define ROTATEFILTER_HPP

#include <math.h>

#include "../base/base.hpp"

class RotateFilter : public BaseFilter {
    int angleOfRotation;
public:
    RotateFilter(const int angleOfRotation = 30);

    QColor calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const override;

    QString getName() override;
};

RotateFilter::RotateFilter(const int _angleOfRotation ) : angleOfRotation(_angleOfRotation) {

}

QColor RotateFilter::calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const {
    QColor resultColor = QColor(0,0,0);

    int centerX = image.width() / 2;
    int centreY = image.height() / 2;

    int coordinateNewX = (coordinateX - centerX) * cos(angleOfRotation) - (coordinateY - centreY) * sin(angleOfRotation) + centerX;
    int coordinateNewY = (coordinateX - centerX) * sin(angleOfRotation) + (coordinateY - centreY) * cos(angleOfRotation) + centreY;

    if (coordinateNewX > 0 && coordinateNewY > 0)
        if (coordinateNewX < image.width() && coordinateNewY < image.height())
            resultColor = image.pixelColor(coordinateNewX, coordinateNewY);

    return resultColor;
}

QString RotateFilter::getName() {
    return QString("Rotate");
}

#endif // ROTATEFILTER_HPP
