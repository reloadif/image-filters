#ifndef PERFECTREFLECTOR_HPP
#define PERFECTREFLECTOR_HPP

#include "../base/base.hpp"

class PerfectReflectorFilter : public BaseFilter {
    int maxRed;
    int maxGreen;
    int maxBlue;

public :
    PerfectReflectorFilter();

    void calculateFields(const QImage& image);

    QColor calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const override;

    QString getName() override;
};


PerfectReflectorFilter::PerfectReflectorFilter() : maxRed(0), maxGreen(0), maxBlue(0) {

}

void PerfectReflectorFilter::calculateFields(const QImage& image) {

    for (int x = 0; x < image.width(); x++) {
        for (int y = 0; y < image.height(); y++) {
            QColor color = image.pixelColor(x, y);

            if (color.red() > maxRed) {
                maxRed = color.red();
            }
            if (color.green() > maxGreen) {
                maxGreen = color.green();
            }
            if (color.blue() > maxBlue) {
                maxBlue = color.blue();
            }

        }
    }



}

QColor PerfectReflectorFilter::calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const {

    QColor resultColor = image.pixelColor( coordinateX, coordinateY );
    resultColor.setRgb( clamp<int>(resultColor.red() * 255.f / maxRed, 0, 255),
                        clamp<int>(resultColor.green() * 255.f / maxGreen, 0, 255),
                        clamp<int>(resultColor.blue() * 255.f / maxBlue, 0, 255) );

    return resultColor;
}

QString PerfectReflectorFilter::getName() {
    return QString("Perfect reflector");
}

#endif // PERFECTREFLECTOR_HPP
