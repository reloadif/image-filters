#ifndef BRIGHTNESSFILTER_HPP
#define BRIGHTNESSFILTER_HPP

#include "../base/base.hpp"

class BrightnessFilter : public BaseFilter {
public :
    QColor calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const override;

    QString getName() override;
};

QColor BrightnessFilter::calculateNewPixelColor(const QImage& image, int coordinateX, int coordinateY) const {
    QColor resultColor = image.pixelColor(coordinateX, coordinateY);
    resultColor.setRgb(
                clamp<int> (resultColor.red() + 70, 0, 255),
                clamp<int> (resultColor.blue() + 70, 0, 255),
                clamp<int> (resultColor.green() + 70, 0, 255)
                );

    return resultColor;
}

QString BrightnessFilter::getName() {
    return  QString("Brightness");
}

#endif // BRIGHTNESSFILTER_HPP
